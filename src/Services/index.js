import axios from 'axios';

/*

    To work in localhost I need to add "domains can make calls to Marvel Comics API"
    https://developer.marvel.com/account

    "Your authorized referrers"

    localhost*
    192.168.0.12*

    And click at "Update" in bottom of page

*/

const publicKey = '520a789f14af6a4e95a81724bb175b34'

const instance = axios.create({
    baseURL: 'https://gateway.marvel.com/v1/public/',
    timeout: 10000,
    method: 'get',
    responseType: 'json'
});

const getHeroes = (char) => {

    const charParam = char ? `nameStartsWith=${char}&` : ''

    return instance({
        url: `/characters?${charParam}apikey=${publicKey}`
    })
    .then(resp => resp)
    .catch(err => err)
}

const getHeroById = (id) => {
    return instance({
        url: `/characters/${id}?&apikey=${publicKey}`
    })
    .then(resp => resp)
    .catch(err => err)
}

const getSeriesByHeroId = (id, limit = 20, offset = 0) => {
    return instance({
        url: `/characters/${id}/series?orderBy=title&limit=${limit}&offset=${offset}&apikey=${publicKey}`
        // characters/1009351/series?orderBy=title&limit=10&offset=5&apikey=520a789f14af6a4e95a81724bb175b34
    })
    .then(resp => resp)
    .catch(err => err)
}

const getComics = () => {
    return instance({
        url: '/comics'
    }).then(resp => {
        console.log(resp);
    }).catch(err => {
        console.log(err);
    })
}

export default {
    getHeroes,
    getHeroById,
    getSeriesByHeroId,
    getComics
}