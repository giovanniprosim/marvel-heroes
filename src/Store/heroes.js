import Services from '../Services'

const initialState = {
    currentHeroListId: null, // id of heroesList array
    heroesList: [],
    heroesEdited: {},
}
  
  const types = {
    SET_HEROES_LIST: 'SET_HEROES_LIST',
    
    SELECT_HERO_LIST_ID: 'SELECT_HERO_LIST_ID',
    EDIT_HERO: 'EDIT_HERO',
  }

  export const actions = {
    getHeroesListByName: (name) => (dispatch) => {
      return Services.getHeroes(name)
        .then(resp => {
          dispatch({type: types.SET_HEROES_LIST, payload: resp.data.data.results})
          return resp.data.data.results
        })
        .catch(resp => console.log(resp))
    },

    selectHero: (heroesListId) => (dispatch) => {
      dispatch({type: types.SELECT_HERO_LIST_ID, payload: heroesListId})
    },

    getHeroById: (id) => (dispatch) => {
      return Services.getHeroById(id)
        .then(resp => {
          return resp.data.data.results
        })
        .catch(resp => console.log(resp))
    },

    getHeroSeries: (id, limit, offset) => (dispatch) => {
      return Services.getSeriesByHeroId(id, limit, offset)
        .then(resp => resp.data.data)
        .catch(resp => console.log(resp))
    },

    editHero: (id, content) => (dispatch) => {
        let hero = {}
        hero[id] = content
        return dispatch({type: types.EDIT_HERO, payload: hero})
    }
  }
  
  export default (state = initialState, { type, payload }) => {
    switch (type) {
      
      case types.SET_HEROES_LIST:
      return {
        ...state,
        heroesList: payload,
      }

      case types.SELECT_HERO_LIST_ID:
      return {
        ...state,
        currentHeroListId: payload,
      }

      case types.EDIT_HERO:
      return {
        ...state,
        heroesEdited: {
            ...state.heroesEdited,
            ...payload
        },
      }

      default:
        return state
    }
  }
  