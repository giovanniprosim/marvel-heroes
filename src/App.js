import React from 'react';

import { Route, Router, Switch } from "react-router-dom"
import { createBrowserHistory } from 'history';

import Form from './Components/Form';
import PageDetails from './Components/PageDetails';

import { GeneralBackGround } from './stylesBasic'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';

import AppReducer from './Store'

let store = createStore(
  AppReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(thunk),
)

function App() {
  return (
    <div className="App">
      <GeneralBackGround />
      <Provider store={store}>
        <Router history={createBrowserHistory()}>
          <Switch>
            <Route exact path='/' component={Form} />
            <Route path='/details/:id' component={PageDetails} />
          </Switch>
        </Router>
      </Provider>
    </div>
  );
}

export default App;
