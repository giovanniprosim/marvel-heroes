export default {

    setItem: (key, content) => window.localStorage.setItem(key, JSON.stringify(content)),
    getItem: (key) => JSON.parse(window.localStorage.getItem(key)),
    removeItem: (key) => window.localStorage.removeItem(key),

}