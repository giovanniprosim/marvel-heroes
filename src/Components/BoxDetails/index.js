import React, { Component } from 'react';

// Styles
import { DetailStyle } from './styles.js';
import { Thumbnail, } from './../../stylesBasic';

// Responsible
import { Grid, Row, Col } from 'react-flexbox-grid'
// //http://www.redbitdev.com/getting-started-with-react-flexbox-grid/

// Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as Heroes} from './../../Store/heroes'

// Components
import InfinityScroll from '../InifinityScroll'
import Loading from '../Loading'

import LocalStorage from './../../Utils/localStorage'

// Images
import editImg from './../../Images/edit.png'
import saveImg from './../../Images/save.png'

class BoxDetails extends Component {

  constructor(props) {
    super(props)

    this.state = {
      isLoading: false,
      onEditMode: false,
      scrollSeries: {
        loaded: 0,
        ended: false,
        series: [],
        qttPerCall: 5,
        total: props.details.series.available
      },
    }
  }

  loadMoreSeries(callback) {
    const { loaded, ended, series, qttPerCall } = this.state.scrollSeries
    const { id } = this.props.details

    if(!ended) {
      this.setState({isLoading: true})

      this.props.actions.getHeroSeries(id, qttPerCall, loaded)
        .then(resp => {

          const isFirst = !loaded

          this.setState({
            scrollSeries: {
              ...this.state.scrollSeries,
              loaded: loaded + resp.count,
              ended: resp.count < qttPerCall,
              series: series.concat(resp.results),
            }
          }, () => callback && callback(isFirst)
        )})
        .catch(resp => console.log(resp))
        .finally(resp => this.setState({isLoading: false}))
    }

  }

  toggleonEditMode() {

    if(this.state.onEditMode) { // save
      LocalStorage.setItem(`hero-${this.props.details.id}`, 
        {...this.props.details, description: this.editedDescription.value}
      )
    }

    this.setState({onEditMode: !this.state.onEditMode}, 
      () => {
        if(this.state.onEditMode) {
          this.textAreaChangeHeight()
          this.state.onEditMode && this.editedDescription.focus()
        }
      }
    )

  }

  textAreaChangeHeight () {
    this.editedDescription.style.height = 0
    this.editedDescription.style.height = this.editedDescription.scrollHeight + 'px'
  }

  infinitySeriesScroll() {
    const { series } = this.state.scrollSeries
    return (
      <InfinityScroll 
        loadMore={(callback) => this.loadMoreSeries(callback)}
        series={series}
      />
    )
  }

  render() {
    const { details } = this.props
    const detailsLocalStorage = LocalStorage.getItem(`hero-${details.id}`) || {}

    return (
      <DetailStyle>
        <Grid fluid>
          
          <Row className="hero-info" middle="sm">
            <Col xs={12} sm={6} className="hero-thumb">
              <Thumbnail src={`${details.thumbnail.path}/standard_xlarge.jpg`} alt={`image-${details.name}`}/>
              <div className="name-character title">{details.name}</div>
            </Col>
            <Col xs={12} sm={6}>
              {
                this.state.onEditMode ? 
                (
                  <div>
                    <div>
                      <textarea 
                        ref={e => {
                          this.editedDescription = e;
                          if(this.state.onEditMode) {
                            ['onchange', 'onkeydown', 'onkeyup', 'onpaste', 'oncut'].forEach(
                              fct => this.editedDescription[fct] = () => this.textAreaChangeHeight()
                            )
                          }
                        }}
                        className="description-text-edit" 
                        defaultValue={detailsLocalStorage.description || details.description}

                      />
                    </div>
                    <div className="description-edit-btn">
                      <img 
                        src={saveImg} 
                        className="icon" 
                        alt="edit" 
                        onClick={() => this.toggleonEditMode()} 
                      />
                    </div>
                  </div>
                )
                :
                (
                  <div>
                    <div className="description-text">
                      {detailsLocalStorage.description || details.description}
                    </div>
                    <div className="description-edit-btn">
                      <img 
                        src={editImg} 
                        className="icon" 
                        alt="edit" 
                        onClick={() => this.toggleonEditMode()} 
                      />
                    </div>
                  </div>
                )
              }

            </Col>       
          </Row>

          { this.state.isLoading && <Loading /> }

          <Row className="series-row">
            <Col xs={12}>
              <div className="title">Séries</div>
            </Col>
            <Col xs={12} className="inifity-scroll-row">
              {this.infinitySeriesScroll(details.series)}
            </Col>
          </Row>

        </Grid>
      </DetailStyle>
    );
  }
}

function mapStateToProps( {heroes} ) {  
  return {
    ...heroes,
  }
}
  
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      ...Heroes,
    },
    dispatch),
  }
}
  
export default connect(mapStateToProps, mapDispatchToProps)(BoxDetails)