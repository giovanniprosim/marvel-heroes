import styled from 'styled-components'

export const DetailStyle = styled.div`

    padding-top: 15px;

    .hero-info {

        background-color: #d0d0d0;
        padding-right: 0px;

        .hero-thumb {
            padding: 0px;
        }

        .name-character {
            text-align: center;
        }            

        .description-text {
            background-color: white;
            padding: 5px;
        }

        .description-text-edit {
            width: 100%;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
            font-size: 1em;
            background-color: burlywood;

            border: 0 none;
            overflow: hidden;
            outline: none;
            height: auto;
            resize: none;
        }

        .description-edit-btn {
            text-align: center;

            .icon {
                height: 20px;
                cursor:pointer;
                margin-top: 10px;
            }
        }

    }

    .series-row {
        margin-top: 20px;
    }

    .title {
        font-size: 1.2em;
        background: black;
        color: white;
        padding: 3px;
    }

`