import React, { Component } from 'react';

//Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as Heroes} from './../../Store/heroes'

import BoxHero from '../BoxHero';
import Loading from '../Loading'
import { ContainerFluid } from './../../stylesBasic';
import { FormStyle } from './style'

class Form extends Component {
    
  constructor(props) {
      super(props)
      console.log(props)
      this.state = {
          isLoading: false,
          listCharacter: props.heroesList || [],
      };
  }

  handleSubmit(event) {
    event.preventDefault();

    this.setState({
      isLoading: true,
      listCharacter: []
    });
    
    this.props.actions.getHeroesListByName(this.inputName.value)
      .then(resp => this.setState({ listCharacter: resp }))
      .catch(resp => console.log(resp))
      .finally(() => this.setState({isLoading: false}))
  }

  render() {console.log(this.props)
      return (
          <div>
            
            <FormStyle>
                <form onSubmit={(e) => this.handleSubmit(e)}>
                    <input 
                        type="text"
                        placeholder="Insira o nome do herói"
                        ref={e => this.inputName = e}
                    />
                    <button type='submit'>Buscar</button>
                </form>
            </FormStyle>

            { this.state.isLoading && <Loading /> }

            <ContainerFluid>
            {
                this.state.listCharacter.map((item, index) => 
                    <BoxHero key={index}
                        id={item.id}
                        idHeroesList={index}
                        name={item.name}
                        image={item.thumbnail.path}
                        description={item.description}
                    />
                )
            }
            </ContainerFluid>

          </div>
      )
  }

}

function mapStateToProps( {heroes} ) {  
  return {
    ...heroes,
  }
}
  
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      ...Heroes,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Form)