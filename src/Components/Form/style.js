import styled from 'styled-components'

export const FormStyle = styled.div`
    
  form {
    text-align: center;
    padding-top: 15px;

    margin-bottom: 15px;

    input, button {
      font-size: 1.2em;
    }

    input {
      height: 36px;
      border-radius: 5px;
    }

    input[type=text], select {
      padding: 1px 12px;
      border: 1px solid #ccc;
      border-radius: 4px;
    }

    button {
      height: 42px;
    }

  }

    

`