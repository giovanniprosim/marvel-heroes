import React, { Component } from 'react';
import { Container, Box, FrontSide, Thumbnail, BackSide, } from './../../stylesBasic';

import { Link } from "react-router-dom";

//Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as Heroes} from './../../Store/heroes'

import LocalStorage from './../../Utils/localStorage'

class BoxHero extends Component {

  selectHero() {
    this.props.actions.selectHero(this.props.idHeroesList)
  }

  render() {

    const { name, id, description, image, } = this.props
    const heroLocalStorage = LocalStorage.getItem(`hero-${id}`) || {}

    return (
        <Container>
            <Link to={`/details/${id}`} onClick={() => this.selectHero()}>
                <Box>
                    <FrontSide>
                        <Thumbnail src={`${image}/standard_xlarge.jpg`} alt={`image-${name}`}/>
                        <p className="name-character">{name}</p>
                    </FrontSide>
                    <BackSide className="back">
                      <div>
                        <p className="description-character">{
                          heroLocalStorage.description || description
                        }</p>
                      </div>
                    </BackSide>
                </Box>
            </Link>
        </Container>
    );
  }
}

function mapStateToProps( {heroes} ) {  
  return {
    ...heroes,
  }
}
  
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      ...Heroes,
    },
    dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BoxHero)
