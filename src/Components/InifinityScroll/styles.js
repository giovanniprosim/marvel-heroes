import styled from 'styled-components'

export const InfinityScrollX = styled.div`
    display: flex;
    overflow-x: scroll;
    overflow-y: hidden;
    // width: 93vw;
    background-color: white;

    &.infinity-scroll-series {
        
    }

    .description-character {
        padding: 0px
    }
`