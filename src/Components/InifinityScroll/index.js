import React, { Component } from 'react';
import { InfinityScrollX } from './styles.js';
import { Container, Box, FrontSide, Thumbnail, BackSide, } from './../../stylesBasic';

class InfinityScroll extends Component {

  constructor(props) {
    super(props)
    this.seriesScroll = {}
  }

  componentDidMount() {
    this.props.loadMore(() => this.addScrollFunctionInSeriesContainer())
  }

  addScrollFunctionInSeriesContainer() {
    this.seriesScroll = document.querySelector('.infinity-scroll-series')

    this.seriesScroll.addEventListener("scroll", () => {
      let scrollX = this.seriesScroll.scrollLeft;
      if(scrollX >= this.scrollXEnd) {
        this.props.loadMore(
          () => this.setScrollXEnd(this.seriesScroll)
        )
      } 
    })

    this.seriesScroll.addEventListener("resize", () => this.setScrollXEnd(this.seriesScroll))

    this.setScrollXEnd(this.seriesScroll)
  }

  setScrollXEnd(element) {
    this.scrollXEnd = 
    Math.max(element.scrollWidth, element.offsetWidth, element.clientWidth) -
    Math.min(element.scrollWidth, element.offsetWidth, element.clientWidth)

  }

  infinitySeriesScroll(series) {
    return (
      <InfinityScrollX className="infinity-scroll-series">
        {
          series.map((item, index) => (
            <Container key={index}> 
              <Box>
                <FrontSide>
                  <Thumbnail src={`${item.thumbnail.path}/standard_xlarge.jpg`} alt={`image-${item.title}`}/>
                  <p className="name-character">{item.title}</p>
                </FrontSide>
                <BackSide className="back">
                  {
                    item.description ? 
                    <p className="description-character">{item.description}</p>
                    :
                    <div>
                      <Thumbnail src={`${item.thumbnail.path}/standard_xlarge.jpg`} alt={`image-${item.title}`}/>
                      <p className="description-character">Sem descrição</p>
                    </div>
                  }
                </BackSide>  
              </Box>           
            </Container>
          ))
        }
      </InfinityScrollX>
    )
  }

  render() {

    const { series } = this.props
    return (
        this.infinitySeriesScroll(series)
    )
  }
}
  
export default InfinityScroll