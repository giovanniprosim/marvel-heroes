import styled from 'styled-components'

export const LoadingStyle = styled.div`

    .loading {
        position: relative;
        text-align: center;
        z-index: 1000;
        height: 0px;
    }

`