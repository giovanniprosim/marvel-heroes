import React, { Component } from 'react';

import { LoadingStyle } from './style'
import gif from './loading.gif'

class Loading extends Component {

  render() { 
      return (
          <LoadingStyle>
            <div className="loading">
                <img src={gif} alt="loading" />
            </div>
          </LoadingStyle>
      )
  }

}

export default Loading