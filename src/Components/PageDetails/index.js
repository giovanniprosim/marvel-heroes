import React, { Component } from 'react';

//Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {actions as Heroes} from './../../Store/heroes'

import BoxDetails from '../BoxDetails'

import LocalStorage from './../../Utils/localStorage'

class PageDetails extends Component {
    
  constructor(props) {
    super(props);
    this.state = {
        isLoading: true,
        details: [],
        series: [],
    };
  }

  componentDidMount() {

    const { currentHeroListId, heroesList } = this.props
    

    this.setState({isLoading: true})

    if(currentHeroListId) {
      console.log(currentHeroListId)
      this.setState({details: {
        ...heroesList[currentHeroListId],
        ...(LocalStorage.getItem(`hero-${currentHeroListId}`) || {})
      }, isLoading: false})
    }
    else
    this.getHeroByIdAtURL()
      .then(resp => {
        console.log(resp[0], resp[0].id)
        this.setState({details: {
          ...resp[0],
          ...(LocalStorage.getItem(`hero-${resp[0].id}`) || {})
        }, isLoading: false})
      })
      .catch(resp => console.log(resp))

  }

  getHeroByIdAtURL() {

    return new Promise((resolve) => {
      const { getHeroById } = this.props.actions
      const heroUrlId = window.location.pathname.match(/details\/(\d{1,})$/)[1]
      resolve(getHeroById(heroUrlId))
    });

  }

  render() {
    return (
      <div>
          {
            !this.state.isLoading &&
            <BoxDetails details={this.state.details} />
          }
      </div>
    )
  }

}

function mapStateToProps( {heroes} ) {  
  return {
    ...heroes,
  }
}
  
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      ...Heroes,
    },
    dispatch),
  }
}
  
export default connect(mapStateToProps, mapDispatchToProps)(PageDetails)
