import styled from 'styled-components'
import marvelBg from './Images/marvel.jpeg'

export const GeneralBackGround = styled.div`

    height: 100vh;
    width: 100vw;
    display: block;
    position: fixed;
    background-color: black;
    opacity: 0.5;
    z-index: -1; 

    &::after {
        content: "";
        background-image: url(${marvelBg});
        opacity: 0.25;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        position: absolute;
        z-index: -1;   
    }

`

export const ContainerFluid = styled.div`
    max-width: 1200px;
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    margin: 0 auto;
`;

export const Container = styled.div`
    position: relative;
    width: 100%;
    max-width: 300px;
    height: 300px;
    padding: 10px;
    transform-style: preserve-3d;
    perspective: 600px;
    transition: 0.5s;
}
`;

export const FrontSide = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: #000;
    backface-visibility: hidden;
    transform: rotatex(0deg);
    transition: 0.5s;
    border: 1px white solid;
    
    p {
        position: absolute;
        bottom: -5px;
        color: #fff;
        width: 100%;
        text-align: center;
        z-index: 1;
    }

    :after {
        content: '';
        position: absolute;
        left: 0;
        bottom: 0;
        width: 100%;
        height: 10%;
        background: rgba(1, 1, 1, 0.7);
    }
`;

export const BackSide = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: #000;
    backface-visibility: hidden;
    transform: rotatex(180deg);
    transition: 0.5s;
    border: 1px white solid;

    p {
        color: #fff;
        z-index: 1;
        padding: 10px 50px;
        text-align: center;
    }

    img {
        opacity: .5;
    }

`;

export const Box = styled.div`
    width: 300px;
    height: 300px;
    :hover {
        ${FrontSide} {

            transform: rotatex(-180deg); 
        }
    }

    :hover {
        ${BackSide} {

            transform: rotatex(0deg);  
            backface-visibility: visible;
        }
    }
`;



export const Thumbnail = styled.img`
    display: block;
    width: 100%;
    height: auto;
    margin: 0 auto;
`;